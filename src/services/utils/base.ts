import Axios, { AxiosInstance } from 'axios';

class APIBase {
  public api: AxiosInstance;
  public post: Function;
  public get: Function;
  public put: Function;
  public delete: Function;
  public patch: Function;

  constructor(baseURL: string) {
    const api = Axios.create({
      baseURL: baseURL,
      withCredentials: false
    });
    api.interceptors.request.use(this.interceptRequest.bind(api));
    api.interceptors.response.use(
      this.interceptResponse.bind(api),
      this.interceptResponseError.bind(api)
    );
    this.get = api.get.bind(api);
    this.post = api.post.bind(api);
    this.put = api.put.bind(api);
    this.delete = api.delete.bind(api);
    this.patch = api.patch.bind(api);
    this.api = api;
  }

  interceptRequest(config: any) {
    config.headers['Authorization'] = localStorage.getItem('auth-token');
    return config;
  }

  interceptResponse(response: any) {
    return Promise.resolve(response);
  }

  interceptResponseError(err: any) {
    return Promise.reject(err);
  }

  setAuthToken(token: any) {
    this.api.defaults.headers.common['Authorization'] = token;
    localStorage.setItem('auth-token', token);
  }

  removeAuthToken() {
    this.api.defaults.headers.common['Authorization'] = null;
  }
}

export default APIBase;
