import { createDecorator } from 'vue-class-component';

export function Progress() {
  return function(_target, key, descriptor) {
    var original = descriptor.value;
    descriptor.value = function setter() {
      const _this = this;
      const args = [];
      for (let _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
      }

      this.state = {
        ...this.state,
        [`${key}Loading`]: true,
        [`${key}Error`]: false
      };

      let value = original.apply(this, args);
      if (value.then) {
        return value
          .then((data) => {
            this.state = {
              ...this.state,
              [`${key}Loading`]: false,
              [`${key}Error`]: false
            };
            return Promise.resolve(data);
          })
          .catch(e => {
            this.state = {
              ...this.state,
              [`${key}Loading`]: false,
              [`${key}Error`]: e
            };
            return Promise.reject(e)
          });
      }
      else
      {
        return Promise.resolve(value);
      }
    }

  };
}
