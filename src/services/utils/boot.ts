export default (chain: Array<Function>) => {
  const end = () => {
    if ((window as any).loader) {
      (window as any).loader.hide();
    }
  };

  let result;
  chain.forEach(task => {
      result = (result || Promise.resolve()).then(() => task() );
  });
  return result
    .then(end)
    .catch(end);
};
