import APIBase from '../utils/base';
import { Component, Vue, Watch } from 'vue-property-decorator';
import config from 'src/config.json';
import { Progress } from '@/services/utils/decorators';

const api = new APIBase(config.api.miscURL);

@Component
export default class TestApi extends Vue {
  state: Object = {
    countries: [],
    timezones: []
  };

  @Progress()
  getRecaptchaKey() {
    return api
      .get('recaptcha/public-key')
      .then(res => {
        return Promise.resolve(res.data.payload);
      })
      .catch(err => {
        return Promise.reject(err);
      });
  }

  getCountries() {
    return api
      .get('countries', {})
      .then(res => {
        this.state.countries = res.data.payload;
        return Promise.resolve(res.data.payload);
      })
      .catch(err => {
        return Promise.reject(err);
      });
  }

  getTimezones() {
    return api
      .get('timezones', {})
      .then(res => {
        this.state.timezones = res.data.payload;
        console.log(this.state.timezones);
        return Promise.resolve(res.data.payload);
      })
      .catch(err => {
        return Promise.reject(err);
      });
  }
}
