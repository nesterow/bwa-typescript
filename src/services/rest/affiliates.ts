import APIBase from '../utils/base';
import {Component, Vue, Watch} from 'vue-property-decorator';
import config from 'src/config.json';
import { Progress } from '@/services/utils/decorators';
import store from '../store';
import router from 'src/routes';
const api = new APIBase(config.api.baseURL);

@Component
export default class TestApi extends Vue {

    state: Object = {};

    // @Progress()
    login(data: object){
        const request = {...data, recaptcha_response: this.recaptcha,  site_id: config.api.site_id};
        return api.post('auth/login', request).then((res)=>{
            api.setAuthToken(res.data.payload.id);
            store.commit('login');
            router.push('/');
        });
    }

    getUserProfile(){
        return api.get('profile')
            .then(res => {
                console.log(res.data.payload)
                this.state.user = res.data.payload;
                return Promise.resolve(res.data.payload);
            })
            .catch(err => {
                return Promise.reject();
            });
    }

    putProfile(payload) {
        return api
            .put('profile', payload)
            .then(res => {
                return Promise.resolve(res.data);
            })
            .catch(err => {
                return Promise.reject(err);
            });
    }

}