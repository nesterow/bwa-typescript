import { Component } from 'vue-property-decorator';
import { mixins } from 'vue-class-component';
import boot from './utils/boot';
import MiscAPI from './rest/misc';
import TestAPI from './rest/affiliates';
import store from './store';


@Component
class Api extends mixins(TestAPI, MiscAPI) {
  /*
  * Add promises for fetching initial data from the api
  * */
  boot: Array<Function> = [
      this.waitVueReady,
      this.getRecaptcha,
      this.getCountries,
      this.getTimezones,
  ];

  recaptcha: string = '';

  created() {
    boot(this.boot)
      .then(() => {})
      .catch(() => {});

    this.getUserData()
  }


  getUserData(){
      loader.show();
      return boot([
          this.getUserProfile
      ])
  }



  waitVueReady(){
    return new Promise((resolve, reject) => {
        this.$nextTick(()=>{
            setTimeout(resolve, 300);
        });
    })
  }

  getRecaptcha() {
    return new Promise((resolve, reject) => {
      this.getRecaptchaKey().then(data => {

        const verifyRecaptcha = data => {
          this.recaptcha = data;
          resolve();
        };

        const recaptchaError = data => {
            console.log(data)
        };

        let was_open = false;
        let listener = window.addEventListener('click', (ev)=>{
          try
          {
              if (!this.recaptcha && was_open && ev.target.tagName === 'DIV')
                  grecaptcha.execute();
          }
          catch (e)
          {

          }
        });

        window.recaptcha_loaded = () => {
          try
          {
              grecaptcha.render('recaptcha', {
                  sitekey: data.public_key,
                  callback: verifyRecaptcha,
                  errorCallback: recaptchaError,
                  expiredCallback: ()=>{
                      grecaptcha.execute();
                  },
                  size: 'invisible'
              });
              if (!localStorage.getItem('auth-token'))
              {
                  grecaptcha.execute();
              }
              else
              {
                  resolve();
              }
              was_open = true;
          }

          catch (e) {
              resolve();
          }


        };

      });
    });
  }
}

export default new Api();
