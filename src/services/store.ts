import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isAuthenticated: localStorage.getItem('auth-token'),
    drawer: window.innerWidth > 1263
  },
  mutations: {
    logout(state){
      state.isAuthenticated = false;
        window.recaptcha_loaded();
    },
    login(state){
      state.isAuthenticated = localStorage.getItem('auth-token');
    },
    toggleDrawer(state){
      state.drawer = !state.drawer;
    }
  },
  actions: {

  },
});
