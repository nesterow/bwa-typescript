
import {Vue} from 'vue-property-decorator';

export default class FormMixin extends Vue {
    state: object = {};

    getError(field){
        return this.state[field + ':error'] && [ this.state[field + ':error'] ]
    }

}

/*
* Parse server validation errors
* */
function validation(errors: object, status: number){

    if (!errors)
        return false;

    if(Array.isArray(errors) && errors.length)
    {
        return 'error:' + status;
    }

    const err = Object.keys(errors);

    if (!err.length)
        return false;

    try
    {
        let result = {};
        for (let i in err) {
            let name = err[i];
            let error = name + '_' + errors[name][0].code;
            result[name + ':error'] = error;
        }
        return result;
    }
    catch (e)
    {
        return false;
    }


}

/*
* Decorator for Form Submit method
* Sets decorator
* */
export function Send() {
    return function(_target, key, descriptor) {
        var original = descriptor.value;
        descriptor.value = function setter() {
            const _this = this;
            const args = [];
            for (let _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }


            this.state = {
                ...this.state,
                loading: true,
                error: false
            };

            var value = original.apply(this, args);
            if (value.then) {
                return value
                    .then((r) => {
                        this.state = {
                            loading: false,
                            error: false
                        };
                        return Promise.resolve(r)
                    })
                    .catch((e:any) => {
                        let errors = validation(e.response.data.errors, e.response.data.code);

                        if (!errors)
                        {
                            errors = { server_error: 'error:' + e.response.status };
                        }

                        this.state = {
                            ...this.state,
                            ...errors,
                            loading: false,
                            error: true
                        };

                        let keys = Object.keys(errors);
                        keys.forEach((error:any)=>{
                            this.$emit(error, errors[error]);
                        });

                        return Promise.reject(e)
                    });
            }
            else
            {
                return Promise.resolve(value);
            }


        };
    };
}

