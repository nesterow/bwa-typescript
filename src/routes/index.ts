import Vue from 'vue';
import Router from 'vue-router';
import Home from '../views/Home.vue';

import NotFoundPage from 'src/views/pages/404.vue';
import LoginForm from 'src/views/pages/login.vue';
import Profile from 'src/views/profile/index.vue';

import auth_required from './hooks/auth_required';
import autologin from './hooks/autologin';
import store from 'src/services/store';

Vue.use(Router);

export default new Router({
  mode: 'hash',
  base: '/',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Profile,
      beforeEnter: auth_required,
    },
    {
        path: '/login',
        name: 'login',
        component: LoginForm,
        beforeEnter: (to, from, next)=>{
            if (localStorage.getItem('auth-token'))
                return next('/');
            else
                return next();
        },
    },
  {
      path: '/logout',
      name: 'logout',
      beforeEnter: (to, from, next) => {
          localStorage.removeItem('auth-token');
          store.commit('logout');
          return next('/login');
      },
  },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
    },
    {
        path: '*',
        component: NotFoundPage,
    },
  ],
});
