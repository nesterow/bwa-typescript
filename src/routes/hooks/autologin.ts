export default (to: object, from: object, next: Function) => {
    localStorage.setItem('auth-token', to.params.token);
    return next('/dashboard/main')
}