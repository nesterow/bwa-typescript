export default (to: any, from: any, next: Function) => {
    if (localStorage.getItem('auth-token')) return next();
    return next({ path: '/login', query: { redirect: to.fullPath } });
}